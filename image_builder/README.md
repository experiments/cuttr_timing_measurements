# Image Builder

This repository contains [Ansible](https://www.ansible.com/) playbooks and tasks
to run the [kameleon](http://kameleon.imag.fr/) image builder on a
[Grid5000](https://www.grid5000.fr/) node using the
[EnOSlib](https://gitlab.inria.fr/discovery/enoslib) library in order to
generate a [GNU Guix](https://www.gnu.org/software/guix/)-enabled system image.
The provided scripts will also automatically add the image to the list of
available Grid5000 image using `kaenv3`.

## Usage

To build the image, simply run `run`.
Please note that the `Build image` ansible task might take a while to run.

## Dependencies

To install the required dependencies, only Guix with support for
[Inferiors](https://www.gnu.org/software/guix/manual/en/html_node/Inferiors.html).
is needed.
The provided [environment file](./environment.scm) will be loaded by
`run` and will download all needed dependencies.

To run `kameleon` on the node, the `virtualbox` package is needed.
Virtualbox is not (and will not) be available in Debian 10 according to:
https://wiki.debian.org/VirtualBox#Debian_10_.22Buster.22
Virtualbox is (supposedly) needed for kameleon:
https://www.grid5000.fr/w/Environments_creation_using_Kameleon_and_Puppet#Install_Kameleon_and_other_dependencies
Therefore, Debian 9 is needed on the remote host at the moment for building
this image.

### Guix-managed dependencies

If Guix is unavailable or if you do not want to use it, you may instead install
the following:

- ssh
- git (for `git rev-parse HEAD`)

Please refer to the [deployment README](./deployment/README.md) file for a list of
runtime dependencies for the deployment.

## Repository dependencies

Two repositories are used as
[git-subrepos](https://github.com/ingydotnet/git-subrepo) to build the image.
The first one is
[kameleon-recipes](https://gitlab.inria.fr/aljanon/kameleon-recipes) which
contains the kameleon recipes required to build a Guix-enabled image.
The Grid5000 resource reservation and deployment is managed by the
[deployment repository](https://gitlab.inria.fr/aljanon/deployment).
