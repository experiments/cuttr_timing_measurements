;; image_builder
;; Copyright (C) 2019  JANON Alexis

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (guix inferior)
             (guix channels)
             (srfi srfi-1))

(define channels
  (list
   (channel
    (name 'guix)
    (url "https://git.savannah.gnu.org/git/guix.git")
    (commit "ca45da9fc9b1eee399ce4344b18cbb129daeca4c"))
   (channel
    (name 'custom)
    (url "https://gitlab.inria.fr/aljanon/guix-packages.git")
    (commit "f9cfa1c51d5640ef60028882cfcd30ab2dd09ffb"))))

(define inferior
  (inferior-for-channels channels))

(define inferior-specifications->manifest
  (lambda (inferior lst)
    (packages->manifest
     (map (lambda (x)
            (first (apply lookup-inferior-packages inferior x)))
          lst))))

(inferior-specifications->manifest
 inferior
 '(("guix")
   ("coreutils-minimal")
   ("git")
   ("openssh")))
