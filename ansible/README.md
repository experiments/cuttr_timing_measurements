# ansible_experiment

Ansible post-deployment playbook. It copies the current (git) repository to the
remote host (ignoring the `.git` and `results` directories), setups, runs, and
teardowns the experiment, and finally retrieves the result back to the local
computer.

The `setup` must be located at `setup/run` and return an absolute path to a
directory which will be used to contain metadata an experiment results. It can
also accept a path, which it may use to determine the results directory. The
`experiment` script must be located in `experiment/run`, and accept a path as
argument (that path being the one output by the `setup` script). `teardown`
follows the same rules as the `experiment` script. This playbook only needs the
scripts to follow the same semantics.

The following `setup` and `teardown` can be used:

- [setup_experiment](https://gitlab.inria.fr/aljanon-experiment-framework/setup_experiment)
- [teardown_experiment](https://gitlab.inria.fr/aljanon-experiment-framework/teardown_experiment)

## Dependencies

This playbook uses the
[synchronize](https://docs.ansible.com/ansible/latest/modules/synchronize_module.html)
ansible module which uses `rsync` internally. This software is needed on the
local machine, and on all remote hosts.

We assume that the `setup`, `experiment` and `teardown` scripts have their
own way of managing their dependencies.
