#!/usr/bin/python3

# setup_experiment
# Copyright (C) 2019  JANON Alexis

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
from pathlib import Path
from shutil import make_archive, rmtree

from loguru import logger
from py_loguru_wrapper import setup_global_logger, teardown_global_logger
from setup_experiment.additional_setup import additional_setup
from setup_experiment.info import write_info

INFO_DIR_NAME = "setup"


@logger.catch
def main():
    base_dir: Path = Path(sys.argv[1]).resolve()
    results_dir: Path = Path(sys.argv[2]).resolve()
    info_dir: Path = results_dir / INFO_DIR_NAME
    setup_global_logger(info_dir)
    logger.info("Logging setup.")
    write_info(info_dir)
    additional_setup(base_dir, results_dir)
    teardown_global_logger()
    logger.info(f"Archiving {INFO_DIR_NAME} directory...")
    make_archive(results_dir / INFO_DIR_NAME, "xztar", root_dir=info_dir)
    logger.info(f"{INFO_DIR_NAME} archived. Cleaning up...")
    rmtree(info_dir)
    logger.info("Cleanup complete.")


if __name__ == "__main__":
    main()
